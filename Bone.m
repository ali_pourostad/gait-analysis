classdef Bone
    %BONE Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        name;
        j1_name;
        j2_name;
        side;
        line_fig;
    end
    
    methods
        function obj = Bone(name, joint1, joint2, side)
            %BONE Construct an instance of this class
            %   Detailed explanation goes here
            obj.name = name;
            obj.j1_name = joint1.name;
            obj.j2_name = joint2.name;
%             if(and(joint1.side == 'L', joint2.side == 'L'))
%                 obj.side = 'L';
%             elseif(and(joint1.side == 'R', joint2.side == 'R'))
%                 obj.side = 'R';
%             else
%                 obj.side = 'N';
%             end
            obj.side = side;
        end
    end
end

