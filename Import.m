function varargout = Import(varargin)
% IMPORT MATLAB code for Import.fig
%      IMPORT, by itself, creates a new IMPORT or raises the existing
%      singleton*.
%
%      H = IMPORT returns the handle to a new IMPORT or the handle to
%      the existing singleton*.
%
%      IMPORT('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in IMPORT.M with the given input arguments.
%
%      IMPORT('Property','Value',...) creates a new IMPORT or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before Import_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to Import_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help Import

% Last Modified by GUIDE v2.5 23-Nov-2019 00:06:44

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @Import_OpeningFcn, ...
                   'gui_OutputFcn',  @Import_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before Import is made visible.
function Import_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to Import (see VARARGIN)

% Choose default command line output for Import
handles.output = hObject;
handles.Experiment = [];
handles.Experiment(1).joints = [Joint('first', [0 0 0])];
handles.Experiment(1).bones = [Bone('___', handles.Experiment(1).joints(1), handles.Experiment(1).joints(1), 'Left')];
% Update handles structure
guidata(hObject, handles);

% UIWAIT makes Import wait for user response (see UIRESUME)
% uiwait(handles.figure1);

function new_h = update_table(handles)
s = size(handles.Experiment(1).bones);
table_data = cell(s(2),3);
for i=1:s(2)
    table_data(i,1) = {handles.Experiment(1).bones(i).name};
    table_data(i,2) = handles.Experiment(1).bones(i).j1_name;
    table_data(i,3) = handles.Experiment(1).bones(i).j2_name;
    table_data(i,4) = handles.Experiment(1).bones(i).side;
end
set(handles.bones_table, 'Data', table_data);
new_h = handles;

% --- Outputs from this function are returned to the command line.
function varargout = Import_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in load_btn.
function load_btn_Callback(hObject, eventdata, handles)
% hObject    handle to load_btn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
[file_name, file_path] = uigetfile('.mat');
joints = load([file_path file_name]);
set(handles.file_txt, 'String', file_name);
s = size(joints.Sub1.Info.Label);
exp_size = size(joints.Sub1.Data);
for j=1:exp_size(2)
    handles.Experiment(j).name = joints.Sub1.Data(j).TaskName;
    handles.Experiment(j).time = joints.Sub1.Data(j).MPS(:,1);
    handles.Experiment(j).joints = [Joint('first', [0 0 0])];
    handles.Experiment(j).bones = [Bone('___', handles.Experiment(1).joints(1), handles.Experiment(1).joints(1), 'Left')];
    handles.joint_names = cell(1, round(s(2)/3));

    for i=1:round(s(2)/3)
        joint_name = joints.Sub1.Info.Label(i*3-1);
        handles.joint_names(i) = joint_name;
        joint_pos = joints.Sub1.Data(j).MPS(:,i*3-1: i*3+1);
        handles.Experiment(j).joints(i) = Joint(joint_name, joint_pos);
    end
end
set(handles.joint1_popup, 'String', handles.joint_names);
set(handles.joint2_popup, 'String', handles.joint_names);
guidata(hObject, handles);

% --- Executes on selection change in joint1_popup.
function joint1_popup_Callback(hObject, eventdata, handles)
% hObject    handle to joint1_popup (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns joint1_popup contents as cell array
%        contents{get(hObject,'Value')} returns selected item from joint1_popup


% --- Executes during object creation, after setting all properties.
function joint1_popup_CreateFcn(hObject, eventdata, handles)
% hObject    handle to joint1_popup (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in joint2_popup.
function joint2_popup_Callback(hObject, eventdata, handles)
% hObject    handle to joint2_popup (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns joint2_popup contents as cell array
%        contents{get(hObject,'Value')} returns selected item from joint2_popup


% --- Executes during object creation, after setting all properties.
function joint2_popup_CreateFcn(hObject, eventdata, handles)
% hObject    handle to joint2_popup (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in side_popup.
function side_popup_Callback(hObject, eventdata, handles)
% hObject    handle to side_popup (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns side_popup contents as cell array
%        contents{get(hObject,'Value')} returns selected item from side_popup


% --- Executes during object creation, after setting all properties.
function side_popup_CreateFcn(hObject, eventdata, handles)
% hObject    handle to side_popup (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in add_btn.
function add_btn_Callback(hObject, eventdata, handles)
% hObject    handle to add_btn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
bone_name = get(handles.bone_name_edit, 'String');
joint1_num = get(handles.joint1_popup, 'Value');
joint2_num = get(handles.joint2_popup, 'Value');
s = size(handles.Experiment);
for j=1:s(2)
    joint1 = handles.Experiment(j).joints(joint1_num);
    joint2 = handles.Experiment(j).joints(joint2_num);
    side = get(handles.side_popup, 'String');
    side = side(get(handles.side_popup, 'Value'));
    if(strcmp(handles.Experiment(j).bones(1).name, '___'))
        handles.Experiment(j).bones(1) = Bone(bone_name, joint1, joint2, side);
    else
        handles.Experiment(j).bones(end+1) = Bone(bone_name, joint1, joint2, side);
    end
end
handles = update_table(handles);
guidata(hObject, handles);

% --- Executes on button press in save_btn.
function save_btn_Callback(hObject, eventdata, handles)
% hObject    handle to save_btn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
[file_name, file_path] = uiputfile('.mat');
output = handles.Experiment;
save([file_path, file_name], 'output');

% --- Executes on button press in load_preset_btn.
function load_preset_btn_Callback(hObject, eventdata, handles)
% hObject    handle to load_preset_btn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
[file_name, file_path] = uigetfile('.mat');
handles.Experiment = load([file_path, file_name]);
handles.Experiment = handles.Experiment.output;
handles = update_table(handles);
guidata(hObject, handles);

% --- Executes on button press in finish_btn.
function finish_btn_Callback(hObject, eventdata, handles)
% hObject    handle to finish_btn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
Gait(handles.Experiment);


function bone_name_edit_Callback(hObject, eventdata, handles)
% hObject    handle to bone_name_edit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of bone_name_edit as text
%        str2double(get(hObject,'String')) returns contents of bone_name_edit as a double


% --- Executes during object creation, after setting all properties.
function bone_name_edit_CreateFcn(hObject, eventdata, handles)
% hObject    handle to bone_name_edit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
