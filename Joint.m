classdef Joint
    %JOINT Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        name = 'joint';
        current = 1;
        pos;
        side;
    end
    
    methods
        function obj = Joint(name, positions)
            %JOINT Construct an instance of this class
            %   Detailed explanation goes here
            obj.name = name;
            obj.pos = positions;
            if(obj.pos(obj.current,2) > 0) 
                obj.side = 'L';
            else
                obj.side = 'R';
            end
        end
        
        function obj = next_frame(obj)
            %NEXT_FRAME draw joint next frame
            %   Detailed explanation goes here
            obj.current = obj.current + 1;
%             obj.fig.XData = obj.pos(obj.current,1);
%             obj.fig.YData = obj.pos(obj.current,2);
        end
    end
end

