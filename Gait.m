function varargout = Gait(varargin)
% GAIT MATLAB code for Gait.fig
%      GAIT, by itself, creates a new GAIT or raises the existing
%      singleton*.
%
%      H = GAIT returns the handle to a new GAIT or the handle to
%      the existing singleton*.
%
%      GAIT('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in GAIT.M with the given input arguments.
%
%      GAIT('Property','Value',...) creates a new GAIT or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before Gait_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to Gait_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help Gait

% Last Modified by GUIDE v2.5 29-Dec-2019 12:19:47

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @Gait_OpeningFcn, ...
                   'gui_OutputFcn',  @Gait_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before Gait is made visible.
function Gait_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to Gait (see VARARGIN)

% Choose default command line output for Gait
handles.output = hObject;
handles.x_rotate = get(handles.x_rotate_slider, 'Value');
handles.y_rotate = get(handles.y_rotate_slider, 'Value');
handles.Angles = [];

handles.Experiment = cell2mat(varargin(1));
s = size(handles.Experiment);
% exp_names = zeros(1,s(2));
for i=1:s(2)
    t = cell2mat(handles.Experiment(i).name);
    st = size(t);
    exp_names(i,1:st(2)) = t;
end
set(handles.exp_popup, 'String', exp_names);

% List of Bones
handles.bones_list = {};
for i = 1:length(handles.Experiment(1).bones)
    handles.bones_list(i) = cellstr(handles.Experiment(1).bones(i).name);
end
set(handles.bone1_popup, 'String', handles.bones_list);
set(handles.bone2_popup, 'String', handles.bones_list);

bone_joints = [handles.Experiment(1).bones(1).j1_name, handles.Experiment(1).bones(1).j2_name];
set(handles.bone1_joint, 'String', bone_joints);
set(handles.bone2_joint, 'String', bone_joints);

fig = handles.axes1;
fig.Box='off';
fig.XTickLabel='';
fig.YTickLabel='';
fig.ZTickLabel='';
%     fig.XLabel.String = 'X';
%     fig.YLabel.String = 'Y';
%     fig.ZLabel.String = 'Z';
fig.XColor='White';
fig.YColor='White';
fig.ZColor='White';

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes Gait wait for user response (see UIRESUME)
% uiwait(handles.figure1);

function ID = find_joint_ID(handles, exp_num, name)
for i = 1:length(handles.Experiment(exp_num).joints)
    if strcmp(handles.Experiment(exp_num).joints(i).name, name)
        ID = i;
        return;
    end
end
ID = -1;

function handles = init_bones(handles, exp_num)
A = [0];
for i = 1:length(handles.Experiment(exp_num).bones)
    current_bone = handles.Experiment(exp_num).bones(i);
    if(strcmp(current_bone.side, 'Left'))
        current_bone.line_fig = plot3(handles.axes1, A, A, A, ...
            'LineWidth',5,'Color','c');
    elseif(strcmp(current_bone.side, 'Right'))
        current_bone.line_fig = plot3(handles.axes1, A, A, A, ...
            'LineWidth',5,'Color','g');
    else
        current_bone.line_fig = plot3(handles.axes1, A, A, A, ...
            'LineWidth',5,'Color','k');
    end
    handles.Experiment(exp_num).bones(i) = current_bone;
end

function update_bones(handles, exp_num, index, dir)
for i = 1:length(handles.Experiment(exp_num).bones)
    current_bone = handles.Experiment(exp_num).bones(i);
    j1 = find_joint_ID(handles, exp_num, current_bone.j1_name);
    j2 = find_joint_ID(handles, exp_num, current_bone.j2_name);
    
    j1_x = handles.Experiment(exp_num).joints(j1).pos(index, dir(1));
    j2_x = handles.Experiment(exp_num).joints(j2).pos(index, dir(1));
    
    j1_y = handles.Experiment(exp_num).joints(j1).pos(index, dir(2));
    j2_y = handles.Experiment(exp_num).joints(j2).pos(index, dir(2));
    
    j1_z = handles.Experiment(exp_num).joints(j1).pos(index, dir(3));
    j2_z = handles.Experiment(exp_num).joints(j2).pos(index, dir(3));
    
    current_bone.line_fig.XData = [j1_x j2_x];
    current_bone.line_fig.YData = [j1_y j2_y];
    current_bone.line_fig.ZData = [j1_z j2_z];
end


function handles = init_angles(handles, show_time)
list_numbers = get(handles.plot_list, 'Value');
angles_plot =  handles.Angles(list_numbers);
angles_num = length(angles_plot);
try
    handles.angles_figure = figure(handles.angles_figure);
catch
    handles.angles_figure = figure('Name', 'Angles');
    set(handles.angles_figure, 'Position', [900,100, 500,600]);
end
for j = 1:angles_num
    ax1 = subplot(angles_num, 1, j);
    cla;
    handles.angles_handle(j) = animatedline(ax1);
    ylabel(angles_plot(j).name);
    xlabel('Time (s)');
    xlim(ax1, [0,show_time]);
end


function update_angles(handles, line_handles, exp_num, index)
angles_list = handles.Angles(get(handles.plot_list, 'Value'));
array_anlges = struct2cell(angles_list);

Points = handles.Experiment(exp_num).joints(cell2mat(array_anlges(2:end,:)));
if(length(angles_list) < 2)
    Points = Points';
end
angles_size = length(get(handles.plot_list, 'Value'));
for k = 1:angles_size
    joint1 = Points(1,k).pos(index,:);
    joint2 = Points(2,k).pos(index,:);
    joint3 = Points(3,k).pos(index,:);
    
    ab = joint2 - joint1;
    bc = joint3 - joint2;
    
    theta = 180 - acosd(dot(ab,bc)./(norm(ab).*norm(bc)));
    time = handles.Experiment(exp_num).time(index);
    addpoints(line_handles(k), time, theta);
end

% --- Outputs from this function are returned to the command line.
function varargout = Gait_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on selection change in exp_popup.
function exp_popup_Callback(hObject, eventdata, handles)
% hObject    handle to exp_popup (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns exp_popup contents as cell array
%        contents{get(hObject,'Value')} returns selected item from exp_popup


% --- Executes during object creation, after setting all properties.
function exp_popup_CreateFcn(hObject, eventdata, handles)
% hObject    handle to exp_popup (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in view_popup.
function view_popup_Callback(hObject, eventdata, handles)
% hObject    handle to view_popup (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns view_popup contents as cell array
%        contents{get(hObject,'Value')} returns selected item from view_popup


% --- Executes during object creation, after setting all properties.
function view_popup_CreateFcn(hObject, eventdata, handles)
% hObject    handle to view_popup (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in play_check.
function play_check_Callback(hObject, eventdata, handles)
% hObject    handle to play_check (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of play_check
global paused;
paused = 0;
handles.step = str2double(get(handles.step_edit, 'String'));
show_time = str2double(get(handles.play_time_edit, 'String'));
loops = 200 * show_time;
view_pop = get(handles.view_popup, 'Value');
axes(handles.axes1);
cla;

h = scatter3(0,0,0, 200, 'filled', 'b');
exp_num = get(handles.exp_popup, 'Value');
handles = init_bones(handles, exp_num);
if(~isempty(handles.Angles))
    handles = init_angles(handles, show_time);
end
axes(handles.axes1);
grid(handles.axes1, 'off');
X = [-500 900];
Y = [-100;1300];
Z = X.*Y*0;
fig = handles.axes1;
ground = surf(fig, X, Y, Z, 'FaceColor', [0.7 0.7 0.7]);
ground.FaceAlpha = 0.3;
if(view_pop == 4)
    grid(handles.axes1, 'on');
    grid on;
    handles.axes1.GridColor = [0.4 0.6 0.7];
end
hold (handles.axes1,'on');

dir(1) = 1;
dir(2) = 2;
dir(3) = 3;

switch(view_pop)
    case 1
        view(handles.axes1, 180, 0);
        fig.XLim = [-500 900];
        fig.ZLim = [-100 2000];
    case 2
        view(handles.axes1, 90, 0);
        fig.YLim = [-100 1300];
        fig.ZLim = [-100 2000];
    case 3
        view(handles.axes1, 0, 90);
        fig.XLim = [-700 900];
        fig.YLim = [-100 2000];
    case 4
        view(handles.axes1, handles.x_rotate, handles.y_rotate);
        fig.XLim = [-500 900];
        fig.YLim = [-100 1300];
        fig.ZLim = [0 2100];
end

joint_size = size(handles.Experiment(exp_num).joints);
pos = zeros(joint_size(2), 3);
for t = 1:handles.step:loops
    
    if(paused == 1)
        break;
    end
    
    for i = 1:joint_size(2)
        pos(i,:) = handles.Experiment(exp_num).joints(i).pos(t,:);
    end
    set(handles.time_txt, 'String', num2str(round(10*handles.Experiment(exp_num).time(t))/10));
    
    h.XData = pos(:,dir(1));
    h.YData = pos(:,dir(2));
    h.ZData = pos(:,dir(3));
    
    update_bones(handles, exp_num, t, dir);
    if(~isempty(get(handles.plot_list, 'String')))
        update_angles(handles, handles.angles_handle, exp_num, t);
    end
    drawnow;
    if(get(handles.record_check, 'Value') == 1)
        F(round(t/handles.step)+1)=getframe(handles.axes1);
    end
    pause(0.0005);
end
guidata(hObject, handles);
if(get(handles.record_check, 'Value') == 1)
    [file_name, file_path] = uiputfile('.mp4');
    video = VideoWriter(file_name,'MPEG-4');
    video.FrameRate=30;
    open(video)
    writeVideo(video,F)
    close(video)
end


% --- Executes on slider movement.
function x_rotate_slider_Callback(hObject, eventdata, handles)
% hObject    handle to x_rotate_slider (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
handles.x_rotate = get(hObject,'Value');
view(handles.x_rotate, handles.y_rotate);
guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function x_rotate_slider_CreateFcn(hObject, eventdata, handles)
% hObject    handle to x_rotate_slider (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on selection change in angles_popup.
function angles_popup_Callback(hObject, eventdata, handles)
% hObject    handle to angles_popup (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns angles_popup contents as cell array
%        contents{get(hObject,'Value')} returns selected item from angles_popup


% --- Executes during object creation, after setting all properties.
function angles_popup_CreateFcn(hObject, eventdata, handles)
% hObject    handle to angles_popup (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in axis_popup.
function axis_popup_Callback(hObject, eventdata, handles)
% hObject    handle to axis_popup (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns axis_popup contents as cell array
%        contents{get(hObject,'Value')} returns selected item from axis_popup


% --- Executes during object creation, after setting all properties.
function axis_popup_CreateFcn(hObject, eventdata, handles)
% hObject    handle to axis_popup (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in joints_popup.
function joints_popup_Callback(hObject, eventdata, handles)
% hObject    handle to joints_popup (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns joints_popup contents as cell array
%        contents{get(hObject,'Value')} returns selected item from joints_popup


% --- Executes during object creation, after setting all properties.
function joints_popup_CreateFcn(hObject, eventdata, handles)
% hObject    handle to joints_popup (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in add_angle_btn.
function add_angle_btn_Callback(hObject, eventdata, handles)
% hObject    handle to add_angle_btn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in add_joint_btn.
function add_joint_btn_Callback(hObject, eventdata, handles)
% hObject    handle to add_joint_btn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on selection change in plot_list.
function plot_list_Callback(hObject, eventdata, handles)
% hObject    handle to plot_list (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns plot_list contents as cell array
%        contents{get(hObject,'Value')} returns selected item from plot_list


% --- Executes during object creation, after setting all properties.
function plot_list_CreateFcn(hObject, eventdata, handles)
% hObject    handle to plot_list (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in del_btn.
function del_btn_Callback(hObject, eventdata, handles)
% hObject    handle to del_btn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on selection change in bone1_popup.
function bone1_popup_Callback(hObject, eventdata, handles)
% hObject    handle to bone1_popup (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns bone1_popup contents as cell array
%        contents{get(hObject,'Value')} returns selected item from bone1_popup
bone_num = get(hObject,'Value');
set(handles.bone1_joint, 'String',[handles.Experiment(1).bones(bone_num).j1_name, ...
                                   handles.Experiment(1).bones(bone_num).j2_name]);
guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function bone1_popup_CreateFcn(hObject, eventdata, handles)
% hObject    handle to bone1_popup (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in bone1_joint.
function bone1_joint_Callback(hObject, eventdata, handles)
% hObject    handle to bone1_joint (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns bone1_joint contents as cell array
%        contents{get(hObject,'Value')} returns selected item from bone1_joint


% --- Executes during object creation, after setting all properties.
function bone1_joint_CreateFcn(hObject, eventdata, handles)
% hObject    handle to bone1_joint (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in bone2_popup.
function bone2_popup_Callback(hObject, eventdata, handles)
% hObject    handle to bone2_popup (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns bone2_popup contents as cell array
%        contents{get(hObject,'Value')} returns selected item from bone2_popup
bone_num = get(hObject,'Value');
set(handles.bone2_joint, 'String',[handles.Experiment(1).bones(bone_num).j1_name, ...
                                   handles.Experiment(1).bones(bone_num).j2_name]);
guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function bone2_popup_CreateFcn(hObject, eventdata, handles)
% hObject    handle to bone2_popup (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in bone2_joint.
function bone2_joint_Callback(hObject, eventdata, handles)
% hObject    handle to bone2_joint (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns bone2_joint contents as cell array
%        contents{get(hObject,'Value')} returns selected item from bone2_joint


% --- Executes during object creation, after setting all properties.
function bone2_joint_CreateFcn(hObject, eventdata, handles)
% hObject    handle to bone2_joint (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in angle_btn.
function angle_btn_Callback(hObject, eventdata, handles)
% hObject    handle to angle_btn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if get(handles.bone1_joint, 'Value') == 1
    j2 = get(handles.bone1_joint, 'String');
    j2 = j2(1);
    j1 = get(handles.bone1_joint, 'String');
    j1 = j1(2);
    j3 = get(handles.bone2_joint, 'String');
    if strcmp(j3(1), j2)
        j3 = j3(2);
    else
        j3 = j3(1);
    end
else
    j2 = get(handles.bone1_joint, 'String');
    j2 = j2(2);
    j1 = get(handles.bone1_joint, 'String');
    j1 = j1(1);
    j3 = get(handles.bone2_joint, 'String');
    if strcmp(j3(1), j2)
        j3 = j3(2);
    else
        j3 = j3(1);
    end
end
new_angle.name = get(handles.angle_name_edit, 'String');
new_angle.j1 = find_joint_ID(handles, 1, j1);
new_angle.j2 = find_joint_ID(handles, 1, j2);
new_angle.j3 = find_joint_ID(handles, 1, j3);
if(isempty(handles.Angles))
    handles.Angles = new_angle;
else
    handles.Angles(end+1) = new_angle;
end
ar = struct2cell(handles.Angles);
set(handles.plot_list, 'String', ar(1,:));
set(handles.angles_table, 'Data',ar(:,:)');

guidata(hObject, handles);

% --- Executes on selection change in angles_list.
function angles_list_Callback(hObject, eventdata, handles)
% hObject    handle to angles_list (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns angles_list contents as cell array
%        contents{get(hObject,'Value')} returns selected item from angles_list


% --- Executes during object creation, after setting all properties.
function angles_list_CreateFcn(hObject, eventdata, handles)
% hObject    handle to angles_list (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in plot_list.
function listbox3_Callback(hObject, eventdata, handles)
% hObject    handle to plot_list (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns plot_list contents as cell array
%        contents{get(hObject,'Value')} returns selected item from plot_list


% --- Executes during object creation, after setting all properties.
function listbox3_CreateFcn(hObject, eventdata, handles)
% hObject    handle to plot_list (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function play_time_edit_Callback(hObject, eventdata, handles)
% hObject    handle to play_time_edit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of play_time_edit as text
%        str2double(get(hObject,'String')) returns contents of play_time_edit as a double


% --- Executes during object creation, after setting all properties.
function play_time_edit_CreateFcn(hObject, eventdata, handles)
% hObject    handle to play_time_edit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function step_edit_Callback(hObject, eventdata, handles)
% hObject    handle to step_edit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of step_edit as text
%        str2double(get(hObject,'String')) returns contents of step_edit as a double


% --- Executes during object creation, after setting all properties.
function step_edit_CreateFcn(hObject, eventdata, handles)
% hObject    handle to step_edit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on slider movement.
function y_rotate_slider_Callback(hObject, eventdata, handles)
% hObject    handle to y_rotate_slider (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
handles.y_rotate = get(hObject,'Value');
view(handles.x_rotate, handles.y_rotate);
guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function y_rotate_slider_CreateFcn(hObject, eventdata, handles)
% hObject    handle to y_rotate_slider (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on button press in pause_btn.
function pause_btn_Callback(hObject, eventdata, handles)
% hObject    handle to pause_btn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global paused;
paused = 1;
guidata(hObject, handles);


function angle_name_edit_Callback(hObject, eventdata, handles)
% hObject    handle to angle_name_edit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of angle_name_edit as text
%        str2double(get(hObject,'String')) returns contents of angle_name_edit as a double


% --- Executes during object creation, after setting all properties.
function angle_name_edit_CreateFcn(hObject, eventdata, handles)
% hObject    handle to angle_name_edit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in angle_del_btn.
function angle_del_btn_Callback(hObject, eventdata, handles)
% hObject    handle to angle_del_btn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.Angles(handles.table_selected_row) = [];
ar = struct2cell(handles.Angles);
set(handles.plot_list, 'String', ar(1,:));
set(handles.angles_table, 'Data',ar(:,:)');

guidata(hObject, handles);

% --- Executes on button press in save_angles_btn.
function save_angles_btn_Callback(hObject, eventdata, handles)
% hObject    handle to save_angles_btn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
[file_name, file_path] = uiputfile('.mat');
output = handles.Angles;
save([file_path, file_name], 'output');

% --- Executes on button press in load_angles_btn.
function load_angles_btn_Callback(hObject, eventdata, handles)
% hObject    handle to load_angles_btn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
[file_name, file_path] = uigetfile('.mat');
handles.Angles = load([file_path, file_name]);
handles.Angles = handles.Angles.output;
ar = struct2cell(handles.Angles);
set(handles.plot_list, 'String', ar(1,:));
set(handles.angles_table, 'Data',ar(:,:)');
guidata(hObject, handles);

% --- Executes when selected cell(s) is changed in angles_table.
function angles_table_CellSelectionCallback(hObject, eventdata, handles)
% hObject    handle to angles_table (see GCBO)
% eventdata  structure with the following fields (see MATLAB.UI.CONTROL.TABLE)
%	Indices: row and column indices of the cell(s) currently selecteds
% handles    structure with handles and user data (see GUIDATA)
try
    handles.table_selected_row = eventdata.Indices(1);
    guidata(hObject, handles);
catch
end


% --- Executes on button press in record_check.
function record_check_Callback(hObject, eventdata, handles)
% hObject    handle to record_check (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of record_check
