# Gait Analysis Project

In this project, there is two MATLAB GUI's that designed to get a raw data of gait test and analyze that

It has 2 parts

## 1. Data Import (Import.m or ImportApp)
	
	1-1. Load bones created before
		If you've created bones before, you can simply press Load Preset buton and load that file and your data is ready. :)
		There is prepared Preset file "Bone-2.mat"
	
	1-2. Create bones from raw data
		To start with a new data, you should load Sub1.mat file from Load Button at top
		After that, name of joints(markers) load in two popup menus
		Now you can create bones which connects 2 joints together.
		After you added all of bones you want, you can save bones in a .mat file by clicking on Save Preset button.
	
	
After finishing bones press Finish button to go to Gait App
	
2. Gait Analysis (Gait.m or GaitApp)